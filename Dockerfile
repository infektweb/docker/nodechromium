FROM node:lts-alpine3.13

RUN apk add --no-cache \
      git \
      chromium

USER node